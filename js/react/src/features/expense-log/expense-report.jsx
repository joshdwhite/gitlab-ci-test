import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { useTransactions } from './api/transactions';
import { formatCurrency, get } from './lib/utils';
import { EntityProvider } from './contexts/entity-context';
import { useMemo, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';

export default function ExpenseReport({
  entityTypeConstant,
  entityId,
  parentEntityTypeConstant,
  startDate,
  endDate,
}) {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });
  return (
    <QueryClientProvider client={queryClient}>
      <EntityProvider
        entityTypeConstant={entityTypeConstant}
        entityId={entityId}
        parentEntityTypeConstant={parentEntityTypeConstant}
        startDate={startDate}
        endDate={endDate}
      >
        <ExpenseTable parentEntityTypeConstant={parentEntityTypeConstant} />
      </EntityProvider>
    </QueryClientProvider>
  );
}

export function ExpenseTable({ parentEntityTypeConstant }) {
  const { data: transactions = [], isLoading } = useTransactions();
  const [sort, setSort] = useState({
    key: 'date',
    isDescending: true,
  });

  const sortedTransactions = useMemo(
    () => (sort.key ? sortTransactions(transactions, sort) : transactions),
    [sort, transactions]
  );

  function handleSort(key) {
    // Toggle sort direction if same key
    if (sort.key === key) {
      setSort({
        ...sort,
        isDescending: !sort.isDescending,
      });
      return;
    }

    // New sort key provided
    setSort({
      key,
      isDescending: false,
    });
  }

  if (isLoading) {
    return (
      <div>
        <Skeleton className="w-100" count={5} />
      </div>
    );
  }

  if (sortedTransactions.length === 0) {
    return (
      <div className="text-center alert alert-light">
        No transactions have been recorded.
      </div>
    );
  }

  return (
    <table className="table mt-4">
      <thead>
        <tr>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('date')}
            >
              Date
            </button>
          </th>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('payee.Name')}
            >
              Payee
            </button>
          </th>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('description')}
            >
              Description
            </button>
          </th>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('category.Name')}
            >
              Category
            </button>
          </th>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('invoiceNumber')}
            >
              Invoice #
            </button>
          </th>
          <th scope="col">
            <button
              type="button"
              className="btn btn-link text-left px-0 no_spin"
              onClick={() => handleSort('amount')}
            >
              Amount
            </button>
          </th>
          {parentEntityTypeConstant !== undefined && (
            <th scope="col">
              <button
                type="button"
                className="btn btn-link text-left px-0 no_spin"
                onClick={() =>
                  handleSort('parentBaseTableAssociationDescription')
                }
              >
                Association
              </button>
            </th>
          )}
        </tr>
      </thead>
      <tbody>
        {sortedTransactions.map((t) => (
          <tr key={t.transactionId}>
            <td>{new Date(t.date).toLocaleDateString()}</td>
            <td>{t.payee.Name}</td>
            <td>{t.description}</td>
            <td>{t.category.Name}</td>
            <td>{t.invoiceNumber}</td>
            <td>{formatCurrency(t.amount)}</td>
            {parentEntityTypeConstant !== undefined && (
              <td>{t.parentBaseTableAssociationDescription}</td>
            )}
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function sortTransactions(data, sort) {
  const sorted = [...data].sort((a, b) => {
    // Since we're using localeCompare to compare the values for the sort,
    // we convert everything to a string first. And because we need to
    // access nested values, we use a Lodash-inspired get function to retrieve
    // the value at the provided path.
    const reference = String(get(a, sort.key));
    const compare = String(get(b, sort.key));

    return reference.localeCompare(compare);
  });

  if (sort.isDescending) {
    sorted.reverse();
  }

  return sorted;
}
