import { createContext, useContext } from 'react';

const EntityContext = createContext(undefined);

export function EntityProvider({ entityTypeConstant, entityId, parentEntityTypeConstant, startDate, endDate, children }) {
  return (
    <EntityContext.Provider value={{ entityTypeConstant, entityId, parentEntityTypeConstant, startDate, endDate }}>
      {children}
    </EntityContext.Provider>
  );
}

export function useEntity() {
  const context = useContext(EntityContext);

  if (context === undefined) {
    throw new Error('useEntity must be used within an EntityProvider');
  }

  return context;
}
