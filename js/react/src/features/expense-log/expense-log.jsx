import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import { TransactionsTable } from './components/transactions-table';

import { EntityProvider } from './contexts/entity-context';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

export default function App({
  entityTypeConstant,
  entityId,
  startDate,
  endDate,
}) {
  return (
    <QueryClientProvider client={queryClient}>
      <EntityProvider
        entityTypeConstant={entityTypeConstant}
        entityId={entityId}
        startDate={startDate}
        endDate={endDate}
      >
        <TransactionsTable />
      </EntityProvider>
    </QueryClientProvider>
  );
}
