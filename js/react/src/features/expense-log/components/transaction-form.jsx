import { Fragment, useEffect, useId, useState } from 'react';
import { Listbox, RadioGroup } from '@headlessui/react';
import clsx from 'clsx';
import { matchSorter } from 'match-sorter';
import { z } from 'zod';

import { useCategories } from '../api/categories';
import { usePayees } from '../api/payees';
import { useParsAccountingTypes } from '../api/pars-accounting-types';

import { formatDate } from '../lib/utils';

import {
  Combobox,
  ComboboxButton,
  ComboboxInput,
  ComboboxLabel,
  ComboboxOption,
  ComboboxOptions,
} from './ui/combobox';

export function TransactionForm({
  transaction,
  hideSubmitButton = false,
  isLoading = false,
  onSubmit,
  startDate,
  endDate,
  ...props
}) {
  const [values, setValues] = useState({
    amount: transaction?.amount ?? '',
    date: transaction?.date
      ? formatDate(transaction.date)
      : formatDate(new Date()),
    payee: transaction?.payee ?? null,
    category: transaction?.category ?? null,
    parsType: transaction?.parsType ?? null,
    description: transaction?.description ?? '',
    invoiceNumber: transaction?.invoiceNumber ?? '',
    isIncome: transaction?.isIncome ?? false,
    // isInternal is a boolean value but must be initialized as empty string
    // when undefined because the intended behavior is not to have a default
    // value selected when creating a new transaction.
    isInternal: transaction?.isInternal ?? '',
  });
  const [errors, setErrors] = useState();
  const [doShowDateValidator, setDoShowDateValidator] = useState(false);

  const isEditing = transaction !== undefined;

  function handleValueChange(key, value) {
    setValues({
      ...values,
      [key]: value,
    });
  }

    //Detect if date is within date range
    function handleDateChange(key, value)
    {
        if (startDate !== null && endDate !== null) {
            let newDate = new Date(value);
            let defaultDate = new Date("1/1/1900");
            if (newDate > defaultDate &&
                (newDate < new Date(startDate) || newDate > new Date(endDate))) {
                setDoShowDateValidator(true);
            }
            else {
                setDoShowDateValidator(false);
            }
        }
        else {
            setDoShowDateValidator(false);
        }

        handleValueChange(key, value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    setErrors(null);

    const validationErrors = validate(values, startDate, endDate);
    if (validationErrors) {
      setErrors(validationErrors);
      return;
    }

    onSubmit(values);
  }

  return (
    <form className="d-flex flex-column" onSubmit={handleSubmit} {...props}>
      <RadioGroup
        value={values.isIncome}
        className="form-group d-flex align-items-center flex-wrap w-100"
        style={{ columnGap: '1rem' }}
        onChange={(val) => handleValueChange('isIncome', val)}
      >
        <RadioGroup.Label className="w-100">Transaction Type</RadioGroup.Label>
        <RadioGroup.Option value={true} className="flex-grow-1">
          {({ checked }) => (
            <div
              className="btn btn-light w-100 no_spin"
              style={{
                borderColor: checked ? '#9429ef' : '#ced4da',
                backgroundColor: checked && 'rgba(148 41 239 / 4%)',
                color: checked && '#9352ab',
              }}
            >
              Income
            </div>
          )}
        </RadioGroup.Option>
        <RadioGroup.Option value={false} className="flex-grow-1">
          {({ checked }) => (
            <div
              className="btn btn-light w-100 no_spin"
              style={{
                borderColor: checked ? '#9429ef' : '#ced4da',
                backgroundColor: checked && 'rgba(148 41 239 / 4%)',
                color: checked && '#9352ab',
              }}
            >
              Expense
            </div>
          )}
        </RadioGroup.Option>
      </RadioGroup>

      <Input
        label="Amount"
        name="amount"
        type="number"
        step="0.01"
        value={values.amount}
        errors={errors?.amount}
        onChange={(e) =>
          handleValueChange('amount', e.currentTarget.valueAsNumber)
        }
      />

      <Input
        label="Date"
        type="date"
        name="date"
        value={values.date}
        errors={errors?.date}
        onChange={(e) => handleDateChange('date', e.currentTarget.value)}
      />

      <PayeeCombobox
        value={values.payee}
        errors={errors?.payee}
        onChange={(value) => handleValueChange('payee', value)}
      />

      <CategoryCombobox
        value={values.category}
        suggestedCategoryId={!isEditing && values.payee?.RecentCategoryId}
        errors={errors?.category}
        onChange={(value) => handleValueChange('category', value)}
      />

      {values.isIncome && (
        <ParsSelect
          value={values.parsType}
          errors={errors?.parsType}
          onChange={(value) => handleValueChange('parsType', value)}
        />
      )}

      <Input
        label="Description"
        name="description"
        value={values.description}
        errors={errors?.description}
        onChange={(e) =>
          handleValueChange('description', e.currentTarget.value)
        }
      />

      <Input
        label="Invoice #"
        name="invoiceNumber"
        value={values.invoiceNumber}
        errors={errors?.invoiceNumber}
        onChange={(e) =>
          handleValueChange('invoiceNumber', e.currentTarget.value)
        }
      />

      <RadioGroup
        value={values.isInternal}
        className="form-group d-flex align-items-center flex-wrap w-100"
        style={{ columnGap: '1rem' }}
        onChange={(val) => handleValueChange('isInternal', val)}
      >
        <RadioGroup.Label className="w-100">Account Type</RadioGroup.Label>
        <RadioGroup.Option value={true} className="flex-grow-1">
          {({ checked }) => (
            <div
              className="btn btn-light w-100 no_spin"
              style={{
                borderColor: checked ? '#9429ef' : '#ced4da',
                backgroundColor: checked && 'rgba(148 41 239 / 4%)',
                color: checked && '#9352ab',
              }}
            >
              Internal
            </div>
          )}
        </RadioGroup.Option>
        <RadioGroup.Option value={false} className="flex-grow-1">
          {({ checked }) => (
            <div
              className="btn btn-light w-100 no_spin"
              style={{
                borderColor: checked ? '#9429ef' : '#ced4da',
                backgroundColor: checked && 'rgba(148 41 239 / 4%)',
                color: checked && '#9352ab',
              }}
            >
              External
            </div>
          )}
        </RadioGroup.Option>
        {errors?.isInternal?.map((e) => (
          <small key={e} className="w-100 text-danger">
            {e}
          </small>
        ))}
      </RadioGroup>

      { (doShowDateValidator == true) && (<small className="text-warning mb-2">The date selected is outside the specified date range of the report, therefore it will not appear on the current report. </small>)}

      {hideSubmitButton ? null : (
        <button
          type="submit"
          className="btn btn-primary no_spin"
          disabled={isLoading}
        >
          {isLoading && (
            <span
              className="spinner-border spinner-border-sm mr-2"
              role="status"
              aria-hidden="true"
            />
          )}
          {isEditing ? 'Save Changes' : 'Create Transaction'}
        </button>
      )}
    </form>
  );
}

// ---

function Input({ label, errors, ...props }) {
  const id = useId();

  return (
    <div className="form-group">
      {label ? <label htmlFor={id}>{label}</label> : null}
      <input
        id={id}
        className={clsx('form-control', errors && 'border-danger')}
        aria-invalid={Boolean(errors)}
        {...props}
      />
      {errors?.map((e) => (
        <small key={e} className="text-danger">
          {e}
        </small>
      ))}
    </div>
  );
}

// ---

function CategoryCombobox({ value, suggestedCategoryId, errors, onChange }) {
  const { data: categories = [], isLoading } = useCategories();
  const [query, setQuery] = useState('');

  useEffect(
    () => {
      if (!suggestedCategoryId) return;

      const suggestedCategory = categories.find(
        (c) => c.Id === suggestedCategoryId
      );

      onChange(suggestedCategory ?? null);
    },
    // This is a good use case for useEffectEvent, but it's not yet available in
    // a stable release of React. As a result, using useEffect and suppressing the
    // linter warning seems like the best tradeoff at this time. See:
    // https://react.dev/learn/separating-events-from-effects#is-it-okay-to-suppress-the-dependency-linter-instead
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [suggestedCategoryId, categories]
  );

  const filteredCategories =
    query === ''
      ? categories
      : matchSorter(categories, query, { keys: ['Name'] });

  const hasExactMatch = filteredCategories.some(
    (c) => c.Name.localeCompare(query, 'en', { sensitivity: 'base' }) === 0
  );

  return (
    <Combobox value={value} by="Id" nullable onChange={onChange}>
      <ComboboxLabel>Select Category</ComboboxLabel>
      <div className="position-relative">
        <ComboboxInput
          className={errors ? 'border-danger' : ''}
          displayValue={(category) => category?.Name}
          onChange={(event) => setQuery(event.target.value)}
        />
        <ComboboxButton />
      </div>
      <ComboboxOptions>
        {isLoading && (
          <li className="list-group-item text-center">Loading categories...</li>
        )}

        {filteredCategories.map((category) => (
          <ComboboxOption key={category.Id} value={category}>
            {category.Name}
          </ComboboxOption>
        ))}

        {query && !hasExactMatch && !isLoading && (
          <ComboboxOption as={Fragment} key="new" value={{ Name: query }}>
            <div className="d-flex flex-column">
              <small>Create New Category</small>
              <span>{query}</span>
            </div>
          </ComboboxOption>
        )}
      </ComboboxOptions>
      {errors?.map((e) => (
        <small key={e} className="text-danger">
          {e}
        </small>
      ))}
    </Combobox>
  );
}

// ---

function PayeeCombobox({ value, errors, onChange }) {
  const { data: payees = [], isLoading } = usePayees();
  const [query, setQuery] = useState('');

  const filteredPayees =
    query === '' ? payees : matchSorter(payees, query, { keys: ['Name'] });

  const hasExactMatch = filteredPayees.some(
    (c) => c.Name.localeCompare(query, 'en', { sensitivity: 'base' }) === 0
  );

  return (
    <Combobox value={value} by="Id" nullable onChange={onChange}>
      <ComboboxLabel>Select Payee</ComboboxLabel>
      <div className="position-relative">
        <ComboboxInput
          className={errors ? 'border-danger' : ''}
          displayValue={(payee) => payee?.Name}
          onChange={(event) => setQuery(event.target.value)}
        />
        <ComboboxButton />
      </div>
      <ComboboxOptions>
        {isLoading && (
          <li className="list-group-item text-center">Loading payees...</li>
        )}

        {filteredPayees.map((payee) => (
          <ComboboxOption key={payee.Id} value={payee}>
            {payee.Name}
          </ComboboxOption>
        ))}

        {query && !hasExactMatch && !isLoading && (
          <ComboboxOption
            as={Fragment}
            key="new"
            value={{ Name: query, Id: null, Type: 2 }}
          >
            <div className="d-flex flex-column">
              <small>Create New Payee</small>
              <span>{query}</span>
            </div>
          </ComboboxOption>
        )}
      </ComboboxOptions>
      {errors?.map((e) => (
        <small key={e} className="text-danger">
          {e}
        </small>
      ))}
    </Combobox>
  );
}

// ---

function ParsSelect({ value, onChange }) {
  const { data: accountingTypes = [] } = useParsAccountingTypes();

  return (
    <Listbox
      as="div"
      value={value}
      className="position-relative form-group"
      by="Id"
      onChange={onChange}
    >
      <Listbox.Label>PARS Accounting Type</Listbox.Label>
      <Listbox.Button className="position-relative d-block w-100 text-left btn border no_spin">
        {value?.Name ? (
          <span>{value.Name}</span>
        ) : (
          <span className="text-secondary">Select Accounting Type</span>
        )}
        <span className="position-absolute" style={{ right: '20px' }}>
          <i
            className="fa fa-fw fa-chevron-down"
            aria-hidden="true"
            style={{ width: '0.25em', height: '0.25em', color: '#7d7d7d' }}
          />
        </span>
      </Listbox.Button>
      <Listbox.Options
        className="position-absolute list-group w-100 shadow overflow-auto"
        style={{
          zIndex: 9999,
          maxHeight: '16rem',
          marginTop: '10px',
          outline: 'none',
        }}
      >
        <Listbox.Option
          value={null}
          className={({ active }) =>
            clsx('list-group-item text-secondary', active && 'bg-light')
          }
          style={{ cursor: 'pointer' }}
        >
          Select Accounting Type
        </Listbox.Option>
        {accountingTypes.map((type) => (
          <Listbox.Option
            key={type.Id}
            value={type}
            className={({ active }) =>
              clsx('list-group-item', active && 'bg-light')
            }
            style={{ cursor: 'pointer' }}
          >
            {type.Name}
          </Listbox.Option>
        ))}
      </Listbox.Options>
    </Listbox>
  );
}

// ---

const TransactionSchema = z.object({
  amount: z.coerce
    .number()
    .positive({ message: 'Amount cannot be 0' })
    .or(z.number().negative({ message: 'Amount cannot be 0' })),
  date: z.coerce.date({ message: 'Please enter a valid date' }),
  payee: z.object(
    {
      Id: z.string().nullish(), // id is null for new payees
      Name: z.string(),
    },
    { invalid_type_error: 'Required' }
  ),
  category: z.object(
    {
      Id: z.number().nullish(), // id is not present for new categories
      Name: z.string(),
    },
    { invalid_type_error: 'Required' }
  ),
  description: z.string().nullish(),
  invoiceNumber: z.string().nullish(),
  isIncome: z.boolean(),
  isInternal: z.boolean({
    required_error: 'Account type is required',
    invalid_type_error: 'Account type is required',
  }),
});

function validate(input) {
  const result = TransactionSchema.safeParse(input);
  if (!result.success) return result.error.flatten().fieldErrors;
}
