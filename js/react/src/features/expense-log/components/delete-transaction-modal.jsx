import { useState } from 'react';

import { useDeleteTransaction } from '../api/transactions';

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
} from './ui/dialog';

export function DeleteTransactionModal({ transaction }) {
  const [open, setOpen] = useState(false);
  const deleteTransaction = useDeleteTransaction();

  function handleDelete() {
    deleteTransaction.mutate(transaction.transactionId, {
      onSuccess: () => {
        setOpen(false);
      },
    });
  }

  return (
    <Dialog open={open} onOpenChange={(isOpen) => setOpen(isOpen)}>
      <DialogTrigger className="btn btn-link no_spin">Delete</DialogTrigger>

      <DialogContent>
        <DialogHeader>
          <DialogTitle>Delete Transaction</DialogTitle>
        </DialogHeader>
        <DialogBody>
          <p>Are you sure you want to delete this transaction?</p>
          <ul className="list-group-flush pl-0 mt-2">
            <li className="list-group-item d-flex justify-content-between px-0">
              <span>Date</span>
              <span>{new Date(transaction.date).toLocaleDateString()}</span>
            </li>
            <li className="list-group-item d-flex justify-content-between px-0">
              <span>Payee</span>
              <span>{transaction.payee.Name}</span>
            </li>
            <li className="list-group-item d-flex justify-content-between px-0">
              <span>Amount</span>
              <span>{transaction.amount}</span>
            </li>
            {transaction.description ? (
              <li className="list-group-item d-flex justify-content-between px-0">
                <span>Description</span>
                <span>{transaction.description}</span>
              </li>
            ) : null}
          </ul>
        </DialogBody>
        <DialogFooter>
          <button
            type="button"
            className="btn btn-link-secondary no_spin"
            onClick={() => setOpen(false)}
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btn-danger no_spin"
            disabled={deleteTransaction.isLoading}
            onClick={handleDelete}
          >
            {deleteTransaction.isLoading && (
              <span
                className="spinner-border spinner-border-sm mr-2"
                role="status"
                aria-hidden="true"
              />
            )}
            <span>Delete Transaction</span>
          </button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
