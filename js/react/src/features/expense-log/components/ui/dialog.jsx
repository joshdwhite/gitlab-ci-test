import * as React from 'react';
import * as DialogPrimitive from '@radix-ui/react-dialog';
import clsx from 'clsx';

const Dialog = DialogPrimitive.Root;

const DialogTrigger = DialogPrimitive.Trigger;

const DialogPortal = React.forwardRef((props, ref) => (
  <DialogPrimitive.Portal ref={ref} {...props} />
));
DialogPortal.displayName = DialogPrimitive.Portal.displayName;

const DialogOverlay = React.forwardRef((props, ref) => (
  <DialogPrimitive.Overlay
    ref={ref}
    className="modal"
    style={{
      display: 'block',
      backgroundColor: 'rgba(0 0 0 / 60%)',
      overflowY: 'auto',
    }}
    {...props}
  />
));
DialogOverlay.displayName = DialogPrimitive.Overlay.displayName;

const DialogContent = React.forwardRef(({ className, ...props }, ref) => (
  <DialogPortal>
    <DialogOverlay>
      <div className="modal-dialog">
        <DialogPrimitive.Content
          ref={ref}
          className={clsx('modal-content', className)}
          {...props}
        />
      </div>
    </DialogOverlay>
  </DialogPortal>
));
DialogContent.displayName = DialogPrimitive.Content.displayName;

const DialogHeader = React.forwardRef(
  ({ className, children, ...props }, ref) => (
    <div ref={ref} className={clsx('modal-header', className)} {...props}>
      {children}
      <DialogPrimitive.Close asChild>
        <button type="button" className="close" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </DialogPrimitive.Close>
    </div>
  )
);
DialogHeader.displayName = 'DialogHeader';

const DialogTitle = React.forwardRef(
  ({ className, children, ...props }, ref) => (
    <DialogPrimitive.Title ref={ref} asChild {...props}>
      <h5 className={clsx('modal-title', className)}>{children}</h5>
    </DialogPrimitive.Title>
  )
);
DialogTitle.displayName = DialogPrimitive.Title.displayName;

const DialogBody = React.forwardRef(({ className, ...props }, ref) => (
  <div ref={ref} className={clsx('modal-body', className)} {...props} />
));
DialogBody.displayName = 'DialogBody';

const DialogFooter = React.forwardRef(({ className, ...props }, ref) => (
  <div ref={ref} className={clsx('modal-footer', className)} {...props} />
));
DialogFooter.displayName = 'DialogFooter';

export {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogBody,
  DialogFooter,
};
