import { Fragment, forwardRef } from 'react';
import { Combobox as ComboboxPrimitive } from '@headlessui/react';
import clsx from 'clsx';

const Combobox = forwardRef(({ className, ...props }, ref) => (
  <ComboboxPrimitive
    ref={ref}
    as="div"
    className={clsx('form-group position-relative', className)}
    {...props}
  />
));
Combobox.displayName = 'Combobox';

// ---

const ComboboxLabel = ComboboxPrimitive.Label;

// ---

const ComboboxButton = forwardRef(({ className, ...props }, ref) => (
  <ComboboxPrimitive.Button
    ref={ref}
    className={clsx('d-flex align-items-center px-2', className)}
    style={{
      all: 'unset',
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 0,
      cursor: 'pointer',
    }}
    {...props}
  >
    <svg
      className="text-secondary"
      viewBox="0 0 20 20"
      fill="currentColor"
      aria-hidden="true"
      style={{ width: '1.25rem', height: '1.25rem' }}
    >
      <path
        fillRule="evenodd"
        d="M10 3a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02L10 4.852 7.3 7.76a.75.75 0 01-1.1-1.02l3.25-3.5A.75.75 0 0110 3zm-3.76 9.2a.75.75 0 011.06.04l2.7 2.908 2.7-2.908a.75.75 0 111.1 1.02l-3.25 3.5a.75.75 0 01-1.1 0l-3.25-3.5a.75.75 0 01.04-1.06z"
        clipRule="evenodd"
      />
    </svg>
  </ComboboxPrimitive.Button>
));
ComboboxButton.displayName = 'ComboboxButton';

// ---

const ComboboxInput = forwardRef(({ className, ...props }, ref) => (
  <ComboboxPrimitive.Input
    ref={ref}
    className={clsx('form-control pr-5', className)}
    autoComplete="off"
    autoCorrect="off"
    {...props}
  />
));
ComboboxInput.displayName = 'ComboboxInput';

// ---

const ComboboxOptions = forwardRef(({ className, ...props }, ref) => (
  <ComboboxPrimitive.Options
    ref={ref}
    className={clsx(
      'position-absolute list-group w-100 shadow overflow-auto',
      className
    )}
    style={{ zIndex: 9999, maxHeight: '16rem', marginTop: '10px' }}
    {...props}
  />
));
ComboboxOptions.displayName = 'ComboboxOptions';

// ---

const ComboboxOption = forwardRef(({ children, ...props }, ref) => (
  <ComboboxPrimitive.Option ref={ref} as={Fragment} {...props}>
    {({ active, selected }) => (
      <li
        className={clsx(
          'list-group-item d-flex align-items-center px-1',
          active && 'bg-light',
          selected && 'font-weight-bold'
        )}
        style={{ cursor: 'pointer ' }}
      >
        <div
          className="d-flex justify-content-center"
          style={{ width: '2rem' }}
        >
          {selected && (
            <svg
              className="text-primary"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
              style={{
                width: '1.25rem',
                height: '1.25rem',
              }}
            >
              <path
                fillRule="evenodd"
                d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                clipRule="evenodd"
              />
            </svg>
          )}
        </div>
        <div>{children}</div>
      </li>
    )}
  </ComboboxPrimitive.Option>
));
ComboboxOption.displayName = 'ComboboxOption';

export {
  Combobox,
  ComboboxInput,
  ComboboxButton,
  ComboboxLabel,
  ComboboxOptions,
  ComboboxOption,
};
