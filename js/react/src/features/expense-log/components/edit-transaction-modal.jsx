import { useState } from 'react';

import { useEntity } from '../contexts/entity-context';

import { useUpdateTransaction } from '../api/transactions';

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
} from './ui/dialog';
import { TransactionForm } from './transaction-form';

export function EditTransactionModal({ transaction }) {
  const [open, setOpen] = useState(false);
  const updateTransaction = useUpdateTransaction();
  const { startDate, endDate } = useEntity();

  function handleSubmit(formInput) {
    const values = {
      transactionId: transaction.transactionId,
      ...formInput,
    };

    updateTransaction.mutate(values, {
      onSuccess: () => {
        setOpen(false);
      },
    });
  }

  return (
    <Dialog open={open} onOpenChange={(isOpen) => setOpen(isOpen)}>
      <DialogTrigger className="btn btn-link no_spin">Edit</DialogTrigger>

      <DialogContent onInteractOutside={(e) => e.preventDefault()}>
        <DialogHeader>
          <DialogTitle>Edit Transaction</DialogTitle>
        </DialogHeader>
        <DialogBody>
          <TransactionForm
            transaction={transaction}
            startDate={startDate}
            endDate={endDate}
            id="editTransactionForm"
            hideSubmitButton={true}
            onSubmit={handleSubmit}
          />
        </DialogBody>
        <DialogFooter>
          <button
            type="button"
            className="btn btn-link-secondary no_spin"
            onClick={() => setOpen(false)}
          >
            Cancel
          </button>
          <button
            type="submit"
            form="editTransactionForm"
            className="btn btn-primary no_spin"
            disabled={updateTransaction.isLoading}
          >
            {updateTransaction.isLoading && (
              <span
                className="spinner-border spinner-border-sm mr-2"
                role="status"
                aria-hidden="true"
              />
            )}
            <span>Save Changes</span>
          </button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
