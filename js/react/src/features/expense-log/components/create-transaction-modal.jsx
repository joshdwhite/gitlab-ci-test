import { useState } from 'react';

import { useCreateTransaction } from '../api/transactions';

import { useEntity } from '../contexts/entity-context';

import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogBody,
  DialogFooter,
  DialogTitle,
} from './ui/dialog';
import { TransactionForm } from './transaction-form';

export function CreateTransactionModal() {
  const [open, setOpen] = useState(false);
  const createTransaction = useCreateTransaction();
  const { entityTypeConstant, entityId, startDate, endDate } = useEntity();

  function handleSubmit(formInput) {
    const values = {
      key: entityId,
      baseFeature: entityTypeConstant,
      ...formInput,
    };

    createTransaction.mutate(values, {
      onSuccess: () => {
        setOpen(false);
      },
    });
  }

  return (
    <Dialog open={open} onOpenChange={(isOpen) => setOpen(isOpen)}>
      <DialogTrigger className="btn btn-primary no_spin">
        Create Transaction
      </DialogTrigger>

      <DialogContent onInteractOutside={(e) => e.preventDefault()}>
        <DialogHeader>
          <DialogTitle>Create Transaction</DialogTitle>
        </DialogHeader>
        <DialogBody>
          <TransactionForm
              id="createTransactionForm"
              hideSubmitButton={true}
              onSubmit={handleSubmit}
              startDate={startDate}
              endDate={endDate}
          />
        </DialogBody>
        <DialogFooter>
          <button
            type="button"
            className="btn btn-link-secondary no_spin"
            onClick={() => setOpen(false)}
          >
            Cancel
          </button>
          <button
            type="submit"
            form="createTransactionForm"
            className="btn btn-primary no_spin"
            disabled={createTransaction.isLoading}
          >
            {createTransaction.isLoading && (
              <span
                className="spinner-border spinner-border-sm mr-2"
                role="status"
                aria-hidden="true"
              />
            )}
            <span>Create Transaction</span>
          </button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
}
