import { useQuery } from '@tanstack/react-query';

import { RequestType } from '../lib/constants';

async function fetchParsAccountingTypes() {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.GetParsAccountingTypes,
      })
  );
  return response.json();
}

export function useParsAccountingTypes() {
  return useQuery({
    queryKey: ['pars-accounting-types'],
    queryFn: fetchParsAccountingTypes,
  });
}
