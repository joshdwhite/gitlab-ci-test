import { useQuery } from '@tanstack/react-query';
import { RequestType } from '../lib/constants';

async function fetchCategories() {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.GetCategories,
      })
  );
  return response.json();
}

export function useCategories() {
  return useQuery({
    queryKey: ['categories'],
    queryFn: fetchCategories,
  });
}
