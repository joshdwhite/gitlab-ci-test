import { useQuery } from '@tanstack/react-query';

import { useEntity } from '../contexts/entity-context';
import { RequestType } from '../lib/constants';

async function fetchPayees({ entityTypeConstant, entityId }) {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.GetPayees,
        key: entityId,
        baseFeatureConstant: entityTypeConstant,
      })
  );
  return response.json();
}

export function usePayees() {
  const { entityTypeConstant, entityId } = useEntity();

  return useQuery({
    queryKey: ['payees'],
    queryFn: () => fetchPayees({ entityTypeConstant, entityId }),
  });
}
