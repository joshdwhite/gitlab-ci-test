import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';

import { useEntity } from '../contexts/entity-context';

import { RequestType } from '../lib/constants';

async function fetchTransactions({
  entityTypeConstant,
  entityId,
  parentEntityTypeConstant,
  startDate,
  endDate
}) {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.GetTransactions,
        key: entityId,
        baseFeatureConstant: entityTypeConstant,
        parentBaseFeatureConstant: parentEntityTypeConstant,
        startDate: startDate,
        endDate: endDate,
      })
  );
  return response.json();
}

export function useTransactions() {
  const {
    entityTypeConstant,
    entityId,
    parentEntityTypeConstant,
    startDate,
    endDate,
  } = useEntity();

  return useQuery({
    queryKey: ['transactions'],
    queryFn: () =>
      fetchTransactions({
        entityTypeConstant,
        entityId,
        parentEntityTypeConstant,
        startDate,
        endDate,
      }),
  });
}

// ---

async function createTransaction(transaction) {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.AddTransaction,
      }),
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(transaction),
    }
  );

  if (!response.ok) throw new Error('Error adding transaction');
}

export function useCreateTransaction() {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: createTransaction,
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ['transactions'] });
    },
  });
}

// ---

async function updateTransaction({
  transaction,
  entityTypeConstant,
  entityId,
}) {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.EditTransaction,
        key: entityId,
        baseFeatureConstant: entityTypeConstant,
        accountingTransactionId: transaction.transactionId,
      }),
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(transaction),
    }
  );

  if (!response.ok) throw new Error('Error updating transaction');
}

export function useUpdateTransaction() {
  const queryClient = useQueryClient();
  const { entityTypeConstant, entityId } = useEntity();

  return useMutation({
    mutationFn: (transaction) =>
      updateTransaction({ transaction, entityTypeConstant, entityId }),
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ['transactions'] });
    },
  });
}

// ---

async function deleteTransaction({
  transactionId,
  entityTypeConstant,
  entityId,
}) {
  const response = await fetch(
    'manage_transactions.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.DeleteTransaction,
        key: entityId,
        baseFeatureConstant: entityTypeConstant,
        accountingTransactionId: transactionId,
      }),
    {
      method: 'POST',
    }
  );

  if (!response.ok) throw new Error('Error deleting transaction');
}

export function useDeleteTransaction() {
  const queryClient = useQueryClient();
  const { entityTypeConstant, entityId } = useEntity();

  return useMutation({
    mutationFn: (transactionId) =>
      deleteTransaction({ transactionId, entityTypeConstant, entityId }),
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ['transactions'] });
    },
  });
}
