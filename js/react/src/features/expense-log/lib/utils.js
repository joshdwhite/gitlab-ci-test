import { formatISO, isDate } from 'date-fns';

export function formatDate(date) {
  const d = isDate(date) ? date : new Date(date);
  return formatISO(d, { representation: 'date' });
}

// ---

export function formatCurrency(amount) {
  const a = parseFloat(amount);

  if (isNaN(a)) throw new Error('Could not parse amount.');

  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(a);
}

// --

export function get(obj, path, defaultValue = undefined) {
  const travel = (regexp) =>
    String.prototype.split
      .call(path, regexp)
      .filter(Boolean)
      .reduce(
        (res, key) => (res !== null && res !== undefined ? res[key] : res),
        obj
      );

  const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);

  return result === undefined || result === obj ? defaultValue : result;
}
