export const RequestType = {
  None: 0,
  GetTransactions: 1,
  AddTransaction: 2,
  GetCategories: 3,
  GetPayees: 4,
  DeleteTransaction: 5,
  EditTransaction: 6,
  GetParsAccountingTypes: 7,
};
