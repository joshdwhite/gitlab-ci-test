import { useState } from 'react';
import { formatDistanceToNow } from 'date-fns';

import { useInterval } from '../../hooks/use-interval';

import { useStats } from './get-stats';

import '@/tailwind.css';

export default function SponsorStats({ passId, isAdmin, sin }) {
  const { data, isError, isLoading, isValidating, mutate } = useStats({
    passId,
    isAdmin,
    sin,
  });

  const [timeSinceRefresh, setTimeSinceRefresh] = useState();

  const metrics = data?.metrics;
  const retrievedAt = data?.timestamp ? new Date(data.timestamp) : null;

  function refreshStats() {
    mutate();
  }

  useInterval(
    () =>
      setTimeSinceRefresh(
        retrievedAt
          ? formatDistanceToNow(retrievedAt, { addSuffix: true })
          : null
      ),
    60
  );

  if (isLoading) {
    return (
      <div className="tw-py-6 tw-text-center">
        <span className="sr-only">Stats are loading</span>
        {/* Use FontAwesome here to match other spinners on home menu */}
        <i
          className="fa fa-refresh fa-spin"
          style={{ fontSize: '2.5rem', color: '#666666' }}
          aria-hidden="true"
        />
      </div>
    );
  }

  if (isError) {
    return (
      <div className="tailwind-reset">
        <div className="tw-flex tw-flex-col tw-items-center tw-gap-y-6 tw-py-6 tw-text-center">
          <p>Could not retrieve stats.</p>
          <button
            type="button"
            className="tw-rounded tw-bg-[#d14b53] tw-px-4 tw-py-2 tw-text-white hover:tw-underline"
            onClick={refreshStats}
          >
            {isValidating ? 'Getting Stats...' : 'Try Again'}
          </button>
          <p>
            Please contact{' '}
            <a
              href="contact_us.aspx?Subject=home menu stats not loading"
              className="tw-text-[#d14b53]"
            >
              eeds support
            </a>{' '}
            if the error persists.
          </p>
        </div>
      </div>
    );
  }

  return (
    <div className="tailwind-reset tw-relative">
      <div className="tw-relative tw-grid tw-grid-cols-1 tw-gap-px tw-overflow-hidden tw-bg-[#00000020] lg:tw-grid-cols-2">
        {Object.entries(metrics).map(([key, value]) => (
          <Section key={key} title={value.header}>
            {value.stats.map((stat) => (
              <Stat
                key={stat.label}
                label={stat.label}
                value={stat.value}
                url={stat.url}
              />
            ))}
          </Section>
        ))}
      </div>
      <div className="tw-absolute tw-bottom-0 tw-right-0 tw-flex tw-flex-col tw-items-end tw-gap-x-1 tw-p-2 tw-text-xs">
        {timeSinceRefresh ? (
          <div className="tw-text-gray-400">updated {timeSinceRefresh}</div>
        ) : null}
        <button
          type="button"
          className="tw-flex tw-items-center tw-gap-x-1 tw-bg-transparent tw-text-[#d14b53] disabled:tw-cursor-not-allowed disabled:tw-text-gray-400"
          disabled={isValidating}
          onClick={refreshStats}
        >
          <i
            className={`fa fa-refresh ${isValidating && 'fa-spin'}`}
            aria-hidden="true"
          />
          <span>{isValidating ? 'updating...' : 'refresh'}</span>
        </button>
      </div>
    </div>
  );
}

function Section({ title, children }) {
  return (
    // We don't know ahead of time how many rows there will be, but there will
    // always be two columns at the md and up breakpoint. This means that there
    // can be times when there's only one populated column in the last row,
    // resulting in an empty column beside it. To avoid that, we target any
    // widow columns by checking to see if a column is both the last child and
    // odd, which works since the widow will always be odd.
    <div className="tw-bg-white tw-py-4 tw-px-6 md:last:odd:tw-col-span-2">
      <h5 className="tw-text-lg tw-font-medium tw-text-gray-700">{title}</h5>
      <div className="tw-mt-4 tw-flex tw-flex-col tw-flex-wrap tw-gap-6 sm:tw-flex-row">
        {children}
      </div>
    </div>
  );
}

function Stat({ label, value, url }) {
  // Add commas between digits for numbers
  const formattedValue =
    typeof value === 'number' ? value.toLocaleString() : value;

  return (
    <div className="tw-flex tw-flex-col">
      <p className="tw-text-sm tw-font-semibold tw-text-gray-400">{label}</p>
      <div className="tw-flex tw-items-center tw-gap-x-1 tw-text-2xl tw-font-semibold">
        {url ? (
          <a href={url} className="tw-text-[#d14b53]">
            {formattedValue}
          </a>
        ) : (
          <span>{formattedValue}</span>
        )}
      </div>
    </div>
  );
}
