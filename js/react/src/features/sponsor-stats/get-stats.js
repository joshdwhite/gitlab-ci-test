import axios from 'axios';
import useSWR from 'swr';
import { differenceInMinutes } from 'date-fns';

export function useStats({ passId, isAdmin, sin }) {
  // If there are stats that are less than an hour old, they can be used as
  // fallback data to be displayed while updated stats are fetched in the
  // background. If there aren't any recently cached stats, no fallback data is
  // displayed and the loading indicator is shown.
  const recentlyCachedStats = getRecentlyCachedStats(passId);

  const { data, error, isValidating, mutate } = useSWR(
    ['getSponsorStats', { passId, isAdmin, sin }],
    getStats,
    {
      revalidateOnFocus: false,
      refreshInterval: 300000, // five minutes
      fallbackData: recentlyCachedStats,
    }
  );

  return {
    data,
    isError: error,
    // isLoading is true only when the cache isn't populated (either from a
    // previous response or fallback data from local storage).
    isLoading: !data && !error,
    isValidating, // true during every fetch
    mutate,
  };
}

function getRecentlyCachedStats(passId) {
  const storedStats = localStorage.getItem(`sponsorHomeMenuStats${passId}`);

  if (!storedStats) {
    return;
  }

  const cachedStats = JSON.parse(storedStats);
  const cachedAt = new Date(cachedStats.timestamp);
  const minutesSinceCached = differenceInMinutes(new Date(), cachedAt);

  // If the cached stats are older than 60 minutes, they aren't considered
  // recent and so aren't returned.
  if (minutesSinceCached > 60) {
    return;
  }

  return cachedStats;
}

async function getStats(key, { passId, isAdmin, sin }) {
  const response = await axios.get('ajax_functions.aspx', {
    params: {
      Function_ID: 118,
      PassID: passId,
      Is_Admin: isAdmin,
      SIN: sin,
    },
  });

  const transformedResponse = transformResponse(response.data);

  // Store the stats so we can potentially use them as fallback data
  localStorage.setItem(
    `sponsorHomeMenuStats${passId}`,
    JSON.stringify(transformedResponse)
  );

  return transformedResponse;
}

function transformResponse(rawResponse) {
  // Make a copy of the response schema that we'll populate with the stats
  const data = JSON.parse(JSON.stringify(transformedResponseSchema));

  // Loop through all possible stats. If the user has access to it (i.e., it was
  // returned from the server), add it to the data sent to the component.
  for (const statMetadata of statsMetadata) {
    const { label } = statMetadata;
    const value = rawResponse[statMetadata.valueAccessor];
    const url = rawResponse[statMetadata.urlAccessor];

    // If a value for the stat wasn't included in the response, skip it.
    // Explicitly check for undefined rather than any falsy value since the stat
    // should still be included if even if its value is 0.
    if (value === undefined) continue;

    // If the value is an array, skip it here since it will be handled later
    if (Array.isArray(value)) continue;

    data.metrics[statMetadata.metric].stats.push({
      label,
      value,
      url,
    });
  }

  // Handle export stats separately since they're in an array
  if ('Export_Stats' in rawResponse) {
    for (const exportStat of rawResponse.Export_Stats) {
      data.metrics.exports.stats.push({
        label: exportStat.Name.replace('<br />', ''),
        value: exportStat.Stat,
        url: exportStat?.URL,
      });
    }
  }

  // Remove metrics without any stats
  Object.entries(data.metrics).forEach(([key, value]) => {
    if (value.stats.length === 0) {
      delete data.metrics[key];
    }
  });

  data.timestamp = rawResponse.JSON_Time_Stamp;

  return data;
}

// This is the structure of the transformed response, and thus represents the
// shape of the data that gets sent to the component. It is cloned and populated
// with the stats available to the user.
const transformedResponseSchema = {
  timestamp: '',
  metrics: {
    activities: {
      header: 'Activities',
      stats: [],
    },
    accounts: {
      header: 'Accounts',
      stats: [],
    },
    evaluations: {
      header: 'Evaluations',
      stats: [],
    },
    outcomes: {
      header: 'Outcomes',
      stats: [],
    },
    disclosures: {
      header: 'Disclosures',
      stats: [],
    },
    coi: {
      header: 'COI Mitigation',
      stats: [],
    },
    exports: {
      header: 'Exports',
      stats: [],
    },
    stark: {
      header: 'Stark Reporting',
      stats: [],
    },
  },
};

// This holds information about each stat so that we know how to access its data
// from the raw reponse and where it belongs in the transformed response schema.
const statsMetadata = [
  // Activities
  {
    label: 'Activities',
    valueAccessor: 'Num_Activities',
    urlAccessor: 'Num_Activities_URL',
    metric: 'activities',
  },
  {
    label: 'Sign-Ins',
    valueAccessor: 'Num_Signins',
    urlAccessor: 'Num_Signins_URL',
    metric: 'activities',
  },
  {
    label: 'Sign-Ins Today',
    valueAccessor: 'Num_Signins_Today',
    urlAccessor: 'Num_Signins_Today_URL',
    metric: 'activities',
  },
  {
    label: 'Unique Attendees',
    valueAccessor: 'Num_Unique_Attendees',
    urlAccessor: 'Num_Unique_Attendees_URL',
    metric: 'activities',
  },
  // Accounts
  {
    label: 'Pending Review',
    valueAccessor: 'Num_Temp_Accounts',
    urlAccessor: 'Num_Temp_Accounts_URL',
    metric: 'accounts',
  },
  {
    label: 'Pending Review (Added by Me)',
    valueAccessor: 'Num_Temp_Accounts_for_Coordinator',
    urlAccessor: 'Num_Temp_Accounts_for_Coordinator_URL',
    metric: 'accounts',
  },
  // Evaluations
  {
    label: 'Responses',
    valueAccessor: 'Num_Evaluation_Responses',
    urlAccessor: 'Num_Evaluation_Responses_URL',
    metric: 'evaluations',
  },
  {
    label: 'Response Rate',
    valueAccessor: 'Percent_Evaluation_Response_Rate',
    urlAccessor: 'Percent_Evaluation_Response_Rate_URL',
    metric: 'evaluations',
  },
  // Outcomes
  {
    label: 'Sent',
    valueAccessor: 'Num_Outcome_Surveys_Sent',
    urlAccessor: 'Num_Outcome_Surveys_Sent_URL',
    metric: 'outcomes',
  },
  {
    label: 'Received',
    valueAccessor: 'Num_Outcome_Surveys_Returned',
    urlAccessor: 'Num_Outcome_Surveys_Returned_URL',
    metric: 'outcomes',
  },
  // Disclosures
  {
    label: 'Completed',
    valueAccessor: 'Num_Completed_Disclosure_Count',
    urlAccessor: 'Num_Completed_Disclosure_Count_URL',
    metric: 'disclosures',
  },
  {
    label: 'Missing',
    valueAccessor: 'Num_Missing_Disclosure_Count',
    urlAccessor: 'Num_Missing_Disclosure_Count_URL',
    metric: 'disclosures',
  },
  {
    label: 'Pending',
    valueAccessor: 'Num_Pending_Disclosure_Count',
    urlAccessor: 'Num_Pending_Disclosure_Count_URL',
    metric: 'disclosures',
  },
  {
    label: 'Awaiting Review',
    valueAccessor: 'Num_Pending_Review_Count',
    urlAccessor: 'Num_Pending_Review_Count_URL',
    metric: 'disclosures',
  },
  {
    label: 'Awaiting Speaker Review',
    valueAccessor: 'Num_Awaiting_Speaker_Review_Count',
    urlAccessor: 'Num_Awaiting_Speaker_Review_Count_URL',
    metric: 'disclosures',
  },
  // COI Mitigation
  {
    label: 'Missing',
    valueAccessor: 'Num_COI_Resolution_Missing',
    urlAccessor: 'Num_COI_Resolution_Missing_URL',
    metric: 'coi',
  },
  {
    label: 'Incomplete',
    valueAccessor: 'Num_COI_Resolution_Incomplete',
    urlAccessor: 'Num_COI_Resolution_Incomplete_URL',
    metric: 'coi',
  },
  // Stark
  {
    label: 'Staff',
    valueAccessor: 'Approaching_Staff_Stark_Limit',
    urlAccessor: 'Approaching_Staff_Stark_Limit_URL',
    metric: 'stark',
  },
  {
    label: 'Non-Staff',
    valueAccessor: 'Approaching_Non_Staff_Stark_Limit',
    urlAccessor: 'Approaching_Non_Staff_Stark_Limit_URL',
    metric: 'stark',
  },
];
