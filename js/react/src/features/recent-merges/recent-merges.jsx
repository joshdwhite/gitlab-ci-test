import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { formatDistanceToNow } from 'date-fns';

import { useMergeRequests } from './api/merge-requests';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      staleTime: Infinity,
    },
  },
});

export default function RecentMerges() {
  return (
    <QueryClientProvider client={queryClient}>
      <MergeRequests />
    </QueryClientProvider>
  );
}

function MergeRequests() {
  const { data: mergeRequests = [], isLoading, isError } = useMergeRequests();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error</div>;
  }

  return (
    <div className="border rounded overflow-hidden">
      <ul
        className="list-group list-group-flush overflow-auto"
        style={{ maxHeight: '500px' }}
      >
        {mergeRequests.map((mergeRequest) => (
          <MergeRequest key={mergeRequest.id} mergeRequest={mergeRequest} />
        ))}
      </ul>
    </div>
  );
}

function MergeRequest({ mergeRequest }) {
  const supportTicketIds = extractSupportTicketIds(mergeRequest.description);

  return (
    <li
      className="list-group-item d-flex justify-content-between"
      style={{ gap: '1rem' }}
    >
      <div>
        <a
          href={mergeRequest.web_url}
          className="d-block h5 mb-0"
          target="_blank"
          rel="noreferrer"
        >
          {mergeRequest.title}
        </a>
        <time
          dateTime={mergeRequest.merged_at}
          className="font-light"
          style={{ fontSize: '0.875rem' }}
        >
          {formatDistanceToNow(new Date(mergeRequest.merged_at), {
            addSuffix: true,
          })}
        </time>
        {supportTicketIds.length > 0 && (
          <div
            className="d-flex flex-wrap align-items-center mt-2"
            style={{ gap: '0.25rem' }}
          >
            {supportTicketIds.map((ticketId) => (
              <a
                key={ticketId}
                href={`/interface_support_ticket.aspx?mode=view&Support_Ticket_ID=${ticketId}`}
                target="_blank"
                rel="noreferrer"
                className="badge badge-primary d-flex items-center"
              >
                <i className="fa fa-ticket mr-1" />
                <span>{ticketId}</span>
              </a>
            ))}
          </div>
        )}
      </div>
      <div
        className="rounded-circle overflow-hidden flex-shrink-0"
        style={{ width: '65px', height: '65px' }}
      >
        <img
          src={mergeRequest.author.avatar_url}
          alt={mergeRequest.author.name}
          className="w-100 h-100"
          style={{ objectFit: 'cover' }}
        />
      </div>
    </li>
  );
}

function extractSupportTicketIds(str) {
  // Regular expression to match all values surrounded by double opening and
  // closing brackets.
  const regex = /<<([^>]+)>>/g;
  const matches = str.match(regex);

  // If there are any matches, map over them to extract the captured values from
  // each match by slicing the string from index 2 to -2 to remove the double
  // opening and closing brackets.
  if (matches) {
    return matches.map((match) => match.slice(2, -2));
  }

  return [];
}
