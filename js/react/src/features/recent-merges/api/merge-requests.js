import { useQuery } from '@tanstack/react-query';

async function fetchRecentMerges() {
  const res = await fetch('/ajax_functions.aspx?Function_ID=177', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: new URLSearchParams({
      GitLab_URL:
        'https://gitlab.com/api/v4/projects/4778248/merge_requests?state=merged',
    }),
  });

  if (!res.ok) {
    throw new Error(`Error fetching merge requests: ${res.status}`);
  }

  return res.json();
}

export function useMergeRequests() {
  return useQuery({
    queryKey: ['mergeRequests'],
    queryFn: fetchRecentMerges,
  });
}
