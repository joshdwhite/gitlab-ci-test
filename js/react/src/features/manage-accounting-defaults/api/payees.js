﻿import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { RequestType } from '../lib/constants';

import { useEntity } from '../contexts/entity-context';

async function fetchPayees() {
    const response = await fetch(
        'manage_accounting_defaults.aspx?' +
        new URLSearchParams({
            apiConstant: RequestType.GetPayees,
        })
    );
    return response.json();
}

export function usePayees() {
    return useQuery({
        queryKey: ['payees'],
        queryFn: fetchPayees,
    });
}


async function setPayeeActiveStatus({ payeeId, isActive, sin }) {
    const response = await fetch(
        'manage_accounting_defaults.aspx?' +
        new URLSearchParams({
            apiConstant: RequestType.SetPayeeActiveStatus,
            sin: sin,
            isActive: isActive,
            payeeId: payeeId
        }),
        {
            method: 'POST',
        }
    );

    if (!response.ok) throw new Error('Error setting payee active status');
}

export function useSetPayeeActiveStatus() {
    const queryClient = useQueryClient();
    const { sin } = useEntity();

    return useMutation({
        mutationFn: ({ payeeId, isActive }) =>
            setPayeeActiveStatus({ payeeId, isActive, sin }),
    //    onSettled: () => {
    //        queryClient.invalidateQueries({ queryKey: ['payees'] });
    //    },
    });
}
