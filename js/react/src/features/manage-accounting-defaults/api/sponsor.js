﻿import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { RequestType } from '../lib/constants';

import { useEntity } from '../contexts/entity-context';

async function fetchSponsorInfo() {
    const response = await fetch(
        'manage_accounting_defaults.aspx?' +
        new URLSearchParams({
            apiConstant: RequestType.GetSponsorInfo,
        })
    );
    return response.json();
}

export function useSponsorInfo() {
    return useQuery({
        queryKey: ['sponsorInfo'],
        queryFn: fetchSponsorInfo,
    });
}
