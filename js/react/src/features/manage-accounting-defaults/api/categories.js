﻿import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { RequestType } from '../lib/constants';

import { useEntity } from '../contexts/entity-context';

async function fetchCategories() {
    const response = await fetch(
        'manage_accounting_defaults.aspx?' +
        new URLSearchParams({
            apiConstant: RequestType.GetCategories,
        })
    );
    return response.json();
}

export function useCategories() {
    return useQuery({
        queryKey: ['categories'],
        queryFn: fetchCategories,
    });
}


async function setCategoryActiveStatus({ categoryId, isActive, sin }) {
  const response = await fetch(
    'manage_accounting_defaults.aspx?' +
      new URLSearchParams({
        apiConstant: RequestType.SetCategoryActiveStatus,
        sin: sin,
        categoryId: categoryId,
        isActive: isActive,
      }),
    {
      method: 'POST',
    }
  );

  if (!response.ok) throw new Error('Error setting category active status');
}

export function useSetCategoryActiveStatus() {
  const queryClient = useQueryClient();
  const { sin } = useEntity();

  return useMutation({
      mutationFn: ({ categoryId, isActive }) =>
      setCategoryActiveStatus({ categoryId, isActive, sin }),
  //  onSettled: () => {
  //    queryClient.invalidateQueries({ queryKey: ['categories'] });
  //  },
  });
}
