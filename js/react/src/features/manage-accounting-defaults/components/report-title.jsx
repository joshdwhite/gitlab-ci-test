import { useSponsorInfo } from '../api/sponsor';

export function ReportTitle({ title })
{
    //Get the data, rename it to sponsor, default it to an empty array so it isn't undefined
    const { data: sponsorInfo = [], isLoading } = useSponsorInfo();

    return (
        <div className='form-group'>
            <h2 className="mt-2 mb-0 text-uppercase font-weight-light">
                {title}
            </h2>
            <h4 className="font-weight-light report_control_subtitle" style={{ color:"#6b7277" }}>
                {sponsorInfo.OrganizationDetails}
            </h4>
        </div>
    )
}
