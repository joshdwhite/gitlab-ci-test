﻿
import { Children } from 'react';
import { ContainerSize } from '../lib/constants';


export function ReportBox({title, clientID = "", cardClass= "", links = "", containerSize = ContainerSize.Large, children})
{
    return (
        <div className={containerSize + " mt-2"} id={clientID} >
            <div className={"card " + cardClass}>
                 <div className="card-header">
                    <div className="clearfix">
                            <span className="float-left h5 mb-0">{title}</span>
                            <span className="float-right Report_Title_Link_Area">{links}</span>
                    </div>
                </div>
                <div className="card-body">
                    {children}
                </div>
            </div>
        </div>
    );
}
