﻿
import { Children } from 'react';
import { ContainerSize } from '../lib/constants';


export function ContentBox({title, links = "", outerDivClass = "", containerSize = ContainerSize.Large, children})
{
    return (
        <div className={outerDivClass + " " + containerSize}>
            <div className="card Menu_Card h-100 content_outer_wrapper">
                <div className="card-header content_wrapper_header h5">
                    <div className="clearfix">
                        {title}
                        <span className="float-right Content_Box_Title_Link_Area">{links}</span>
                    </div>
                </div>
                <div className="card-body content_wrapper_data">
                    {children}
                </div>
            </div>
        </div>
    );
}
