import { useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import { usePayees, useSetPayeeActiveStatus } from '../api/payees';

import 'react-loading-skeleton/dist/skeleton.css';

export function ManagePayees()
{
    //Get the data, rename it to payees, default it to an empty array so it isn't undefined
    const { data: payees = [], isLoading } = usePayees();

    return (
        <table className="table table-sm">
            <thead>
                <tr>
                    <th>
                        Payee
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
            <tbody>
            {
                isLoading ?
                (
                    <tr>
                      <td colSpan="7">
                        <Skeleton className="w-100" count={5} />
                      </td>
                    </tr>
                ) :
                (
                    payees.map((p) =>
                    (
                        <tr key={p.Id}>
                            <td>
                                {p.Name}
                            </td>
                            <td>
                                <div className="custom-control custom-switch">
                                    <SetPayeeActive payeeId={p.Id} isActive={p.IsActive} />
                                </div>
                            </td>
                        </tr>
                    ))
                )
            }
            </tbody>
        </table>
    )
}


function SetPayeeActive({ payeeId, isActive }) {

    //Get State Variables
    const [checked, setChecked] = useState(isActive);

    //Get Remote Variables
    const setPayeeActiveStatus = useSetPayeeActiveStatus();

    const handleChange = () => {
        setPayeeActiveStatus.mutate({ payeeId: payeeId, isActive: !checked}, {
            onSuccess: () => {
                setChecked(!checked);
            },
        });
    }; 

    return (
        <>
            <input type="checkbox" className="custom-control-input" id={"payee" + payeeId} defaultChecked={checked} onChange={handleChange} />
            <label className="custom-control-label font-weight-normal" htmlFor={"payee" + payeeId} style={{ cursor: 'pointer' }}>
                {checked ? ("appear on list ON") : ("appear on list OFF")}
            </label>
        </>
    );
}
