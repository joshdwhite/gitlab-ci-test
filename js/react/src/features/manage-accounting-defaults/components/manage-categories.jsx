import { useState } from 'react';
import { useCategories, useSetCategoryActiveStatus } from '../api/categories';
import Skeleton from 'react-loading-skeleton';

import 'react-loading-skeleton/dist/skeleton.css';

export function ManageCategories() {
    //Get the data, rename it to categories, default it to an empty array so it isn't undefined
    const { data: categories = [], isLoading } = useCategories();

    return (
        <table className="table table-sm">
            <thead>
                <tr>
                    <th>
                        Category
                    </th>
                    <td>
                    </td>
                </tr>
            </thead>
            <tbody>
            {
                isLoading ?
                (
                    <tr>
                      <td colSpan="7">
                        <Skeleton className="w-100" count={5} />
                      </td>
                    </tr>
                ) :
                (
                    categories.map((c) =>
                    (
                        <tr key={c.Id}>
                            <td>
                                {c.Name}
                            </td>
                            <td>
                                <div className="custom-control custom-switch">
                                    <SetCategoryActive categoryId={c.Id} isActive={c.IsActive} />
                                </div>
                            </td>
                        </tr>
                    ))
                )
            }
            </tbody>
        </table>
    )

}



function SetCategoryActive({ categoryId, isActive }) {

    //Get State Variables
    const [checked, setChecked] = useState(isActive);

    //Get Remote Variables
    const setCategoryActiveStatus = useSetCategoryActiveStatus();

    const handleChange = () => {
        setCategoryActiveStatus.mutate({ categoryId: categoryId, isActive: !checked}, {
            onSuccess: () => {
                setChecked(!checked);
            },
        });
    }; 

    return (
        <>
            <input type="checkbox" className="custom-control-input" id={"category" + categoryId} defaultChecked={checked} onChange={handleChange} />
            <label className="custom-control-label font-weight-normal" htmlFor={"category" + categoryId} style={{ cursor: 'pointer' }}>
                {checked ? ("appear on list ON") : ("appear on list OFF")}
            </label>
        </>
    );
}
