import { createContext, useContext } from 'react';

const EntityContext = createContext(undefined);

export function EntityProvider({ sin, children }) {
    return (
        <EntityContext.Provider value={{ sin }}>
            {children}
        </EntityContext.Provider>
    );
}

export function useEntity() {
    const context = useContext(EntityContext);

    if (context === undefined) {
        throw new Error('useEntity must be used within an EntityProvider');
    }

    return context;
}
