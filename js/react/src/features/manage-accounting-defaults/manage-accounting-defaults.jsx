import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import { ReportTitle } from './components/report-title';
import { ManagePayees } from './components/manage-payees';
import { ManageCategories } from './components/manage-categories';
import { ReportBox } from './components/report-box';
import { ContainerSize } from './lib/constants';

import { EntityProvider } from './contexts/entity-context';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
});

export default function App({ sin }) {
    return (
        <QueryClientProvider client={queryClient}>
            <EntityProvider
                sin={sin}
            >
                <ReportTitle title="Manage Accounting Payees and Categories" />
                <div className="row">
                    <div className="col-lg-6">
                        <ReportBox
                            title="Manage Payees"
                            containerSize={ContainerSize.Maximum}
                        >
                            <ManagePayees />
                        </ReportBox>
                        
                    </div>
                    <div className="col-lg-6">
                        <ReportBox
                            title="Manage Categories"
                            containerSize={ContainerSize.Maximum}
                        >
                            <ManageCategories />
                        </ReportBox>
                    </div>
                </div>
            </EntityProvider>
        </QueryClientProvider>
    );
}
