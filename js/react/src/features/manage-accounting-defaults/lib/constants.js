﻿export const RequestType = {
    None: 0,
    GetPayees: 1,
    GetCategories: 2,
    SetPayeeActiveStatus : 3,
    SetCategoryActiveStatus : 4,
    GetSponsorInfo: 5,
};


export const ContainerSize = {
    None: "",
    Small: "col-12 col-md-10 col-lg-5",
    Medium: "col-12 col-md-10 col-lg-6",
    Large: "col-12 col-md-10",
    Maximum: "col-12 col-md-12",
}
