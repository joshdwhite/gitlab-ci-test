import { useEffect, useMemo, useRef, useState } from 'react';
import { Combobox } from '@headlessui/react';

import { debounce } from '@/utils';

import { Modal } from '@/components';

import { Input } from './components/input';
import { Results } from './components/results';
import { SearchTypeSelect } from './components/search-type-select';

import { useIsOpen } from './hooks/use-is-open';
import { useSearchResults } from './api/get-search-results';

import '@/tailwind.css';

export default function AdminPersonSearch() {
  const [searchString, setSearchString] = useState('');
  const [searchType, setSearchType] = useState('coordinator');
  const [selectedPerson, setSelectedPerson] = useState();

  // Managing the open state of the component is extracted to a custom hook because it
  // contains the logic for displaying the dialog when Ctrl/Cmd + K is pressed
  const [isOpen, setIsOpen] = useIsOpen();

  // Async state (i.e., search results)
  const { results, isLoading, isError } = useSearchResults({
    searchString,
    searchType,
  });

  // Store ref to search input so we can mark it to receive focus when dialog opens
  const searchInput = useRef();

  // Input change handler is debounced to prevent too many calls to API
  const debouncedChangeHandler = useMemo(
    () => debounce((e) => setSearchString(e.target.value.trim()), 300),
    []
  );

  // Navigate to person's profile when selected
  useEffect(() => {
    if (!selectedPerson) return;

    if (selectedPerson?.PassID) {
      window.open(
        `/interface_customer_overview.aspx?PassID=${selectedPerson.PassID}`
      );
      return;
    }

    window.open(`/interface_physician_3.asp?PIN=${selectedPerson.PIN}`);
  }, [selectedPerson]);

  // ** THIS SHOULD ONLY BE TEMPORARY. Vue compponent should be phased out. **
  // Take over old Vue control so that clicking its container opens this. This
  // is accomplished by creating a new element, positioning it over the old
  // component, then listening for clicks on that element and opening the new
  // component whenever a click happens.
  useEffect(() => {
    const vueComponentContainer = document.getElementById('searchRow');

    if (!vueComponentContainer) {
      return;
    }

    // Apply relative positioning to the Vue component so that the absolutely
    // positioned created below will be anchored to it.
    vueComponentContainer.style.position = 'relative';

    const clickInterceptor = document.createElement('div');
    clickInterceptor.style.cssText = `
      position: absolute;
      width: 100%;
      height: 100%;
      z-index: 10;
    `;

    vueComponentContainer.prepend(clickInterceptor);

    function onVueComponentClick() {
      setIsOpen(true);
    }

    clickInterceptor.addEventListener('click', onVueComponentClick);

    return () => {
      clickInterceptor.removeEventListener('keydown', onVueComponentClick);
    };
  }, [setIsOpen]);

  return (
    <Modal
      show={isOpen}
      initialFocus={searchInput}
      onClose={setIsOpen}
      afterLeave={() => setSearchString('')}
    >
      <Combobox
        as="div"
        value={selectedPerson}
        className="tailwind-reset tw-relative tw-mx-auto tw-w-full tw-max-w-xl tw-divide-y tw-overflow-hidden tw-rounded-xl tw-bg-white tw-text-sm tw-shadow-2xl tw-ring tw-ring-black tw-ring-opacity-5"
        onChange={setSelectedPerson}
      >
        <SearchTypeSelect searchType={searchType} onChange={setSearchType} />
        <Input
          ref={searchInput}
          searchString={searchString}
          showSpinner={isLoading}
          onChange={debouncedChangeHandler}
        />

        {isError && (
          <div className="tw-p-3 tw-text-red-600">
            There was an error retrieving results
          </div>
        )}

        {searchString && !isLoading && results?.length === 0 && (
          <p className="tw-p-3">No results found.</p>
        )}

        {results?.length > 0 && <Results results={results} />}
      </Combobox>
    </Modal>
  );
}
