import { useState } from 'react';
import { Combobox } from '@headlessui/react';

import { formatLocation } from '@/utils';

export function Results({ results = [] }) {
  const [showInactives, setShowInactives] = useState(false);
  const isCoordinatorSearch = Object.keys(results[0]).includes('PassID');
  const inactiveCount = isCoordinatorSearch
    ? results.filter((r) => !r.Is_Active).length
    : 0;

  return (
    <div>
      {inactiveCount > 0 && (
        <ToggleInactivesOption
          showInactives={showInactives}
          inactiveCount={inactiveCount}
          onChange={(e) => setShowInactives(e.target.checked)}
        />
      )}

      {results.length > 0 && (
        <Combobox.Options
          className="tw-mb-0 tw-max-h-96 tw-divide-y tw-divide-solid tw-divide-gray-200 tw-overflow-y-auto tw-px-0 tw-py-4 tw-text-sm"
          static
        >
          {results.map((result) => (
            <Option
              key={result.PassID ?? result.PIN}
              value={result}
              showInactives={showInactives}
            />
          ))}
        </Combobox.Options>
      )}
    </div>
  );
}

// ---

function ToggleInactivesOption({ showInactives, inactiveCount, onChange }) {
  return (
    <div className="tw-flex tw-justify-end tw-p-4 tw-pb-0">
      <label className="tw-mb-0 tw-flex tw-items-center tw-gap-x-1 tw-text-sm tw-font-normal">
        <input type="checkbox" checked={showInactives} onChange={onChange} />
        <span className="tw-text-gray-500">
          Include {inactiveCount} Inactives
        </span>
      </label>
    </div>
  );
}

// ---

function Option({ value, showInactives }) {
  const isCoordinator = Object.keys(value).includes('PassID');

  // Don't render anything if this is a coordinator who is inactive and the "show
  // inactives" option hasn't been selected
  if (isCoordinator && !value.Is_Active && !showInactives) {
    return null;
  }

  const OptionComponent = isCoordinator ? CoordinatorOption : ParticipantOption;

  return (
    <Combobox.Option value={value} className="tw-cursor-pointer">
      {({ active }) => (
        <div
          className={`tw-flex tw-items-center tw-justify-between tw-px-4 tw-py-2 ${
            active ? 'tw-bg-purple-600' : 'tw-bg-white'
          }`}
        >
          <OptionComponent person={value} active={active} />
        </div>
      )}
    </Combobox.Option>
  );
}

// ---

function CoordinatorOption({ person, active }) {
  return (
    <>
      <div>
        <div className="tw-flex tw-items-center tw-gap-x-1">
          <div
            className={`${active && 'tw-text-white'} ${
              !person.Is_Active && 'tw-line-through'
            }`}
          >
            {person.Name}
          </div>
          {person.Is_Full_Sponsor && <Badge active={active}>Admin</Badge>}
        </div>
        <div
          className={`tw-text-xs ${
            active ? 'tw-text-purple-200' : 'tw-text-gray-500'
          }`}
        >
          {person.Sponsor_Name}
        </div>
      </div>
      <div
        className={`tw-tabular-nums ${
          active ? 'tw-text-white' : 'tw-text-gray-600'
        }`}
      >
        {person.PassID}
      </div>
    </>
  );
}

// ---

function ParticipantOption({ person, active }) {
  const location = formatLocation(person.City, person.State);

  return (
    <>
      <div>
        <div className={active ? 'text-white' : ''}>{person.Name}</div>
        {location ? (
          <div
            className={`tw-text-xs ${
              active ? 'tw-text-purple-200' : 'tw-text-gray-500'
            }`}
          >
            {location}
          </div>
        ) : null}
      </div>
      <div
        className={`tw-tabular-nums ${
          active ? 'tw-text-white' : 'tw-text-gray-600'
        }`}
      >
        {person.PIN}
      </div>
    </>
  );
}

// --

function Badge({ active, children }) {
  return (
    <span
      className={`tw-rounded-lg tw-px-1 tw-text-xs tw-font-medium ${
        active
          ? 'tw-bg-white tw-text-purple-600'
          : 'tw-bg-purple-50 tw-text-purple-500'
      }`}
    >
      {children}
    </span>
  );
}
