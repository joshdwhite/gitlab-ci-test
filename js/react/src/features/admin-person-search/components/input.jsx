import * as React from 'react';
import { Combobox } from '@headlessui/react';
import { SearchIcon } from '@heroicons/react/outline';

export const Input = React.forwardRef(function InputRoot(
  { searchString, showSpinner, onChange },
  ref
) {
  return (
    <div className="tw-relative tw-flex tw-items-center tw-px-4">
      <SearchIcon className="tw-h-6 tw-w-6 tw-text-gray-500" />
      <Combobox.Input
        ref={ref}
        type="search"
        className="tw-h-12 tw-w-full tw-p-2 focus:tw-outline-none"
        placeholder="Search..."
        displayValue={() => searchString}
        onChange={onChange}
      />
      {showSpinner && <Spinner />}
    </div>
  );
});

// ---

function Spinner() {
  return (
    <svg
      className="tw-absolute tw-right-5 tw-h-5 tw-w-5 tw-animate-spin tw-text-gray-700"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <circle
        className="tw-opacity-25"
        cx="12"
        cy="12"
        r="10"
        stroke="currentColor"
        strokeWidth="4"
      ></circle>
      <path
        className="tw-opacity-75"
        fill="currentColor"
        d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
      ></path>
    </svg>
  );
}
