import { RadioGroup } from '@headlessui/react';

export function SearchTypeSelect({ searchType, onChange }) {
  return (
    <RadioGroup
      value={searchType}
      className="tw-flex tw-items-center tw-gap-2 tw-p-2"
      onChange={onChange}
    >
      <RadioGroup.Label className="tw-sr-only">Search Type</RadioGroup.Label>
      <RadioGroup.Option value="coordinator" className={optionContainer}>
        {({ checked }) => (
          <div className={checked ? optionChecked : optionUnchecked}>
            Coordinator
          </div>
        )}
      </RadioGroup.Option>
      <RadioGroup.Option value="participant" className={optionContainer}>
        {({ checked }) => (
          <div className={checked ? optionChecked : optionUnchecked}>
            Participant
          </div>
        )}
      </RadioGroup.Option>
    </RadioGroup>
  );
}

// Classes

const optionContainer =
  'tw-flex-1 tw-cursor-pointer tw-rounded-lg tw-ring-purple-500 tw-ring-offset-1 focus:tw-outline-none focus:tw-ring-2';
const optionBase =
  'tw-rounded-lg tw-border tw-p-2 tw-text-center tw-transition';
const optionChecked = `${optionBase} tw-border-purple-500 tw-bg-purple-100`;
const optionUnchecked = `${optionBase} tw-border-transparent hover:tw-bg-gray-100`;
