import { Fragment, useMemo, useRef, useState } from 'react';
import { Combobox } from '@headlessui/react';
import clsx from 'clsx';

import { debounce, formatLocation } from '@/utils';

import { useSearch } from './get-participants';

export default function ParticipantSearch({ inputName = 'PIN' }) {
  const [searchString, setSearchString] = useState('');
  const [selectedPeople, setSelectedPeople] = useState([]);
  const isNumericInput = searchString && !isNaN(searchString);

  // Async state (i.e., search results)
  const { results, isWaitingForFullPin, isLoading, isError } = useSearch({
    searchString,
    currentSelections: selectedPeople,
  });

  // Ref to input is used to programmatically set focus
  const inputRef = useRef();

  // Input change handler is debounced to prevent too many calls to API
  const handleInputChange = useMemo(
    () => debounce((e) => setSearchString(e.target.value.trim()), 300),
    []
  );

  const handleValueChange = (value) => {
    setSelectedPeople(value);
    setSearchString(''); // reset query so that menu is closed upon selection
    inputRef.current.focus();
  };

  const deselectPerson = (person) => {
    setSelectedPeople(selectedPeople.filter((p) => p.pin !== person.pin));
  };

  return (
    <div className="container">
      <Combobox value={selectedPeople} onChange={handleValueChange} multiple>
        {({ open }) => (
          <div className="form-group position-relative">
            <Combobox.Label>Attendee Name</Combobox.Label>
            <Combobox.Button as="div">
              <Combobox.Input
                ref={inputRef}
                displayValue={() => searchString}
                className="form-control"
                placeholder="Last Name, First Name or PIN"
                maxLength={isNumericInput ? 8 : 50}
                onChange={handleInputChange}
              />
            </Combobox.Button>
            {searchString && open ? (
              <Combobox.Options
                className="position-absolute w-100 list-group list-group-flush mt-1 border rounded shadow overflow-auto"
                style={{ maxHeight: '24rem', zIndex: '10' }}
                static
              >
                {isWaitingForFullPin ? (
                  <li className="list-group-item">
                    Awaiting eight-digit PIN...
                  </li>
                ) : isLoading ? (
                  <li className="list-group-item">Loading...</li>
                ) : isError ? (
                  <li className="list-group-item">Error retrieving results</li>
                ) : (
                  <OptionsList options={results} />
                )}
              </Combobox.Options>
            ) : null}
          </div>
        )}
      </Combobox>

      {selectedPeople.length > 0 ? (
        <SelectionsList
          selectedPeople={selectedPeople}
          onRemove={deselectPerson}
        />
      ) : null}

      {/* Hidden inputs */}
      {selectedPeople.map((person) => (
        <input
          key={person.pin}
          type="hidden"
          name={inputName}
          value={person.pin}
        />
      ))}
    </div>
  );
}

function OptionsList({ options = [] }) {
  if (options.length === 0) {
    return <li className="list-group-item">No results</li>;
  }

  return (
    <>
      {options.map((option) => (
        <Combobox.Option as={Fragment} key={option.pin} value={option}>
          {({ active }) => (
            <li
              className={clsx([
                'list-group-item',
                active && 'bg-primary text-white',
              ])}
            >
              <div>
                {option?.lastName}, {option?.firstName} {option?.degree}
              </div>
              <div
                className={active ? 'text-white' : 'text-muted'}
                style={{ fontSize: '0.825rem', opacity: active ? 0.8 : 1 }}
              >
                {formatLocation(option.city, option.state)}
              </div>
            </li>
          )}
        </Combobox.Option>
      ))}
    </>
  );
}

function SelectionsList({ selectedPeople, onRemove }) {
  return (
    <div className="mt-4">
      <strong style={{ color: '#653d73' }}>
        Selected {selectedPeople.length === 1 ? 'Attendee' : 'Attendees'}
      </strong>
      <ul className="list-group list-group-flush">
        {selectedPeople.map((person) => (
          <li
            key={person.PIN}
            className="list-group-item px-0 d-flex justify-content-between"
          >
            <div>
              {person?.lastName}, {person?.firstName} {person?.degree}
            </div>
            <button
              type="button"
              className="btn btn-sm btn-outline-danger no_spin"
              onClick={() => onRemove(person)}
            >
              <span className="sr-only">deselect person</span>
              <span aria-hidden="true">&times;</span>
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}
