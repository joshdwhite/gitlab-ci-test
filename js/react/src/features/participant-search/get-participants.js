import { useMemo } from 'react';
import axios from 'axios';
import useSWR from 'swr';

export function useSearch({ searchString, currentSelections }) {
  const trimmedSearchString = searchString.trim();
  const isNumericInput = !isNaN(trimmedSearchString);
  const isEightDigits = isNumericInput && trimmedSearchString.length === 8;
  const waitingForFullPin = isNumericInput && !isEightDigits;
  const isPinSearch = isNumericInput && isEightDigits;

  // Conditionally call search function based on whether there is a searchString
  // and, if it's a PIN search, whether there are eight digits.
  const { data, error } = useSWR(
    searchString && !waitingForFullPin
      ? ['getParticipants', searchString, isPinSearch]
      : null,
    getParticipants
  );

  const dedupedResults = useMemo(() => {
    if (!data) {
      return [];
    }

    function notAlreadySelected(result) {
      return !currentSelections.some(
        (currentSelection) => currentSelection.pin === result.pin
      );
    }

    return data.filter(notAlreadySelected);
  }, [data, currentSelections]);

  return {
    results: dedupedResults,
    isWaitingForFullPin: waitingForFullPin,
    isError: error,
    isLoading: searchString && !waitingForFullPin && !data && !error,
  };
}

async function getParticipants(key, searchString, isPinSearch) {
  let results = [];

  if (isPinSearch) {
    results = await getParticipantByPin(searchString);
  } else {
    results = await getParticipantsByName(searchString);
  }

  return results;
}

async function getParticipantByPin(pin) {
  const response = await axios.get('ajax_functions.aspx', {
    params: {
      Function_ID: 32,
      PIN: pin,
    },
  });

  const results = response.data.Participant_Array;

  // If there aren't any matches, the response will still return an array with
  // an object of the same shape, but its key values will all be empty strings.
  // So, to see if there are no results, we check to see if every key of all of
  // the returned objects is an empty string.
  // const noMatch = results.every((result) =>
  //   Object.values(result).every((value) => !value)
  // );

  // if (noMatch) {
  //   return [];
  // }

  return results.map((result) => ({
    pin: result.p,
    firstName: result.f,
    lastName: result.l,
    degree: result.d,
    city: result.c,
    state: result.t,
  }));
}

async function getParticipantsByName(searchString) {
  const { lastName, firstName } = getNamePartsFromSearchString(searchString);

  const response = await axios.get('ajax_functions.aspx', {
    params: {
      Function_ID: 135,
      First_Name: firstName,
      Last_Name: lastName,
      Do_Search_for_Coordinator: false,
    },
  });

  return response.data.map((result) => ({
    pin: result.PIN,
    firstName: result.First_Name,
    lastName: result.Last_Name,
    degree: result.Degree,
    city: result.City,
    state: result.State,
  }));
}

// Since there's only a single input, we need to parse the first and last names
function getNamePartsFromSearchString(searchString = '') {
  let lastName = '';
  let firstName = '';

  // Determine how to split name based on whether the user typed a comma. If the
  // user typed a comma, split there. Otherwise, split at the first space.
  if (searchString.includes(',')) {
    [lastName, firstName] = searchString.split(',');
  } else {
    const indexOfSpace = searchString.indexOf(' ');
    if (indexOfSpace === -1) {
      lastName = searchString; // No comma or space, so treat everything as last
    } else {
      lastName = searchString.slice(0, indexOfSpace);
      firstName = searchString.slice(indexOfSpace);
    }
  }

  return {
    lastName: lastName.trim(),
    firstName: firstName.trim(),
  };
}
