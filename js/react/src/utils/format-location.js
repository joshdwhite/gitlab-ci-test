export const formatLocation = (city = '', state = '') => {
  if (!city && !state) return;

  if (city && state) {
    return `${city}, ${state}`;
  }

  return city || state;
};
