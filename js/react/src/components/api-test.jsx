import { useState } from 'react';
import Axios from 'axios';
import useSWR from 'swr';

// Create custom Axios instance with preferred defaults
const axios = Axios.create({
  baseURL: window.INTERNAL_API_URL,
  withCredentials: true, // Pass cookies with every request
});

// Return only the data from the response (ignore config, statusCode, etc.)
axios.interceptors.response.use((response) => response.data);

async function fetchPublicData() {
  const data = await axios.get(`/enduring-materials`);

  return data.data.map((d) => ({
    id: d.id,
    name: d.description,
  }));
}

async function fetchProtectedData() {
  const data = await axios.get(`/sponsors`);

  return data.data.map((d) => ({
    id: d.sin,
    name: d.name,
  }));
}

async function fetchData(dataType) {
  return dataType === 'protected' ? fetchProtectedData() : fetchPublicData();
}

export default function ApiTest() {
  const [dataType, setDataType] = useState(null);

  return (
    <div className="mb-4">
      <div className="d-flex">
        <button
          type="button"
          className="btn btn-primary no_spin mr-2"
          onClick={() => setDataType('public')}
        >
          Get Public Data
        </button>
        <button
          type="button"
          className="btn btn-primary no_spin"
          onClick={() => setDataType('protected')}
        >
          Get Protected Data
        </button>
      </div>
      <Results dataType={dataType} onClearResults={() => setDataType(null)} />
    </div>
  );
}

function Results({ dataType, onClearResults }) {
  const { data, error } = useSWR(dataType, fetchData);
  const isLoading = !data && !error;

  if (!dataType) return null;

  if (isLoading)
    return (
      <div>
        <hr />
        Loading...
      </div>
    );

  if (error)
    return (
      <div>
        <hr />
        Error
      </div>
    );

  return (
    <div>
      <hr />
      <button
        type="button"
        className="btn btn-sm btn-outline-primary mb-2"
        onClick={onClearResults}
      >
        Clear
      </button>
      <ul className="list-group">
        {data.map((d) => (
          <li key={d.id} className="list-group-item">
            {d.name}
          </li>
        ))}
      </ul>
    </div>
  );
}
