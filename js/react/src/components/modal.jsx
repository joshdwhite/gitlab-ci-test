import * as React from 'react';
import { Dialog, Transition } from '@headlessui/react';

import '@/tailwind.css';

export function Modal({ show, afterLeave, initialFocus, onClose, children }) {
  // Use relative positioning for the content of the modal. We handle this here
  // so the consumer doesn't have to worry about explicitly specifiying the
  // position. Without this, the stacking order can get messed up, with the
  // contents appearing behind the backdrop. Since the transition is a fragment,
  // it can only have a single child, so using React.Children.only will throw an
  // errror if more than one child is passed.
  const child = React.Children.only(children);
  const childWithRelativePosition = React.cloneElement(child, {
    style: { position: 'relative', ...child.props.style },
  });

  return (
    <Transition.Root show={show} as={React.Fragment} afterLeave={afterLeave}>
      <Dialog
        className="tw-fixed tw-inset-0 tw-z-10 tw-overflow-y-auto tw-p-4 tw-pt-[25vh]"
        initialFocus={initialFocus}
        onClose={onClose}
      >
        <Transition.Child
          as={React.Fragment}
          enter="tw-ease-out tw-duration-300"
          enterFrom="tw-opacity-0"
          enterTo="tw-opacity-100"
          leave="tw-ease-in tw-duration-200"
          leaveFrom="tw-opacity-100"
          leaveTo="tw-opacity-0"
        >
          <div className="tw-fixed tw-inset-0 tw-bg-gray-500/75" />
        </Transition.Child>
        <Transition.Child
          as={React.Fragment}
          enter="tw-ease-out tw-duration-300"
          enterFrom="tw-opacity-0 tw-scale-95"
          enterTo="tw-opacity-100 tw-scale-100"
          leave="tw-ease-in tw-duration-200"
          leaveFrom="tw-opacity-100 tw-scale-100"
          leaveTo="tw-opacity-0 tw-scale-95"
        >
          {/* Making this inline is required in order for clicks on either side
          of the content to trigger the dialog to close. Otherwise, the panel
          takes the full width of the container even if its content doesn't. */}
          <Dialog.Panel className="tw-inline">
            {childWithRelativePosition}
          </Dialog.Panel>
        </Transition.Child>
      </Dialog>
    </Transition.Root>
  );
}
