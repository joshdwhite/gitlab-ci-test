import * as React from 'react';
import { createRoot } from 'react-dom/client';
import 'vite/modulepreload-polyfill';

const AdminPersonSearch = React.lazy(() =>
  import('./src/features/admin-person-search/admin-person-search')
);
document
  .querySelectorAll('[data-react-component="AdminPersonSearch"]')
  .forEach((domContainer) => {
    createRoot(domContainer).render(
      <React.Suspense>
        <AdminPersonSearch />
      </React.Suspense>
    );
  });

// ---

const ParticipantSearch = React.lazy(() =>
  import('./src/features/participant-search/participant-search')
);
document
  .querySelectorAll('[data-react-component="ParticipantSearch"]')
  .forEach((domContainer) => {
    // Get component props from data attributes
    const inputName = domContainer.dataset.inputName;

    createRoot(domContainer).render(
      <React.Suspense>
        <ParticipantSearch inputName={inputName} />
      </React.Suspense>
    );
  });

// ---

const SponsorStats = React.lazy(() =>
  import('./src/features/sponsor-stats/sponsor-stats')
);
document
  .querySelectorAll('[data-react-component="SponsorStats"]')
  .forEach((domContainer) => {
    // Get component props from data attributes
    const passId = domContainer.dataset.passId;
    const isAdmin = domContainer.dataset.isAdmin === 'True'; // Convert "True" or "False" to boolean
    const sin = domContainer.dataset.sin;

    createRoot(domContainer).render(
      <React.Suspense>
        <SponsorStats passId={passId} isAdmin={isAdmin} sin={sin} />
      </React.Suspense>
    );
  });

// ---

const ApiTest = React.lazy(() => import('./src/components/api-test'));
document
  .querySelectorAll('[data-react-component="ApiTest"]')
  .forEach((domContainer) => {
    createRoot(domContainer).render(
      <React.Suspense>
        <ApiTest />
      </React.Suspense>
    );
  });

// ---

const ExpenseLog = React.lazy(() =>
  import('./src/features/expense-log/expense-log')
);
document
  .querySelectorAll('[data-react-component="ExpenseLog"]')
  .forEach((domContainer) => {
    const entityTypeConstant = domContainer.dataset.entityTypeConstant;
    const entityId = domContainer.dataset.entityId;
    const startDate = domContainer.dataset.startDate;
    const endDate = domContainer.dataset.endDate;

    createRoot(domContainer).render(
      <React.Suspense>
        <ExpenseLog
          entityTypeConstant={entityTypeConstant}
          entityId={entityId}
          startDate={startDate}
          endDate={endDate}
        />
      </React.Suspense>
    );
  });


const ExpenseReport= React.lazy(() =>
    import('./src/features/expense-log/expense-report')
);
document
    .querySelectorAll('[data-react-component="ExpenseReport"]')
    .forEach((domContainer) => {
        const entityTypeConstant = domContainer.dataset.entityTypeConstant;
        const entityId = domContainer.dataset.entityId;
        const parentEntityTypeConstant = domContainer.dataset.parentEntityTypeConstant;
        const startDate = domContainer.dataset.startDate;
        const endDate = domContainer.dataset.endDate;

        createRoot(domContainer).render(
            <React.Suspense>
                <ExpenseReport
                    entityTypeConstant={entityTypeConstant}
                    entityId={entityId}
                    parentEntityTypeConstant={parentEntityTypeConstant }
                    startDate={startDate}
                    endDate={endDate}
                />
            </React.Suspense>
        );
    });
// ---

const ManageAccountingDefaults = React.lazy(() =>
  import('./src/features/manage-accounting-defaults/manage-accounting-defaults')
);
document
  .querySelectorAll('[data-react-component="ManageAccountingDefaults"]')
  .forEach((domContainer) => {
    const sin = domContainer.dataset.sin;

    createRoot(domContainer).render(
      <React.Suspense>
        <ManageAccountingDefaults
          sin={sin}
        />
      </React.Suspense>
    );
  });

// ---

const RecentMerges = React.lazy(() =>
  import('./src/features/recent-merges/recent-merges')
);
document
  .querySelectorAll('[data-react-component="RecentMerges"]')
  .forEach((domContainer) => {
    createRoot(domContainer).render(
      <React.Suspense>
        <RecentMerges />
      </React.Suspense>
    );
  });
