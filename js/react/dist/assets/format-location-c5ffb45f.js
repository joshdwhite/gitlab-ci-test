const u=(n,o)=>{let r=null;return(...e)=>{window.clearTimeout(r),r=window.setTimeout(()=>{n.apply(null,e)},o)}},l=(n="",o="")=>{if(!(!n&&!o))return n&&o?`${n}, ${o}`:n||o};export{u as d,l as f};
