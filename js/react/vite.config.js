import { defineConfig, splitVendorChunkPlugin } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';

export default defineConfig(({ command }) => {
  const basePath = command === 'build' ? '/js/react/dist' : '/js/react';

  return {
    plugins: [react(), splitVendorChunkPlugin()],
    base: basePath,
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
      },
    },
    build: {
      manifest: true,
      rollupOptions: {
        input: './entry.jsx',
      },
    },
  };
});
